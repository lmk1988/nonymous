package com.CS4274.anonymous;
import java.util.List;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.CS4274.anonymous.Manager;
import com.CS4274.anonymous.server.*;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.parse.SendCallback;

public class Server {

	//View should not touch server, view should only touch Manager

	private static Server instance;
	private static ParseACL defaultACL;
	
	private static long LEVEL_MULTIPLIER = 2;
	private static int highestLevelReached = 1;
	private static int moderateLevelReached = 1;
	private static int MAX_LEVEL_FOR_DYNAMIC_THREADS = 10;
	private static double LAT_LONG_THRESHOLD = 0.0001;//about 11m
	private static double PULL_RADIUS = 9;//about 100m

	private static int SUB_LOCATION_RADIUS = 10;//about 100m
	private static String locationChannel;
	private static boolean isPushingLocation;
	private static boolean isApplicationClosed;
	
	private static String phoneID;
	private static Context context;
	private static Class<? extends Activity> activity;
	
	private Server(){
		locationChannel = null;
	}

	public static Server getInstance(){
		if(instance==null){
			instance = new Server();

			//ParseUser.enableAutomaticUser();
			defaultACL = new ParseACL();

			// If you would like all objects to be private by default, remove this line.
			defaultACL.setPublicReadAccess(true);
			defaultACL.setPublicWriteAccess(true);			

			ParseACL.setDefaultACL(defaultACL, true);
			ParseInstallation.getCurrentInstallation().saveInBackground();

			phoneID = Manager.getInstance().phoneID;
		}
		return instance;
	}
	public static void registerDevice(Context _context, Class<? extends Activity> _activity){
		context = _context;
		activity = _activity;
	}
	public void getThreads(double longitude, double latitude, ThreadsCallback callback){
		Log.i(this.getClass().getName(),"getThreads, location:"+longitude+","+latitude);
		
		subLocation(longitude,latitude);
		//getThreads(longitude,latitude,callback,moderateLevelReached);
		getThreads(longitude,latitude,callback,highestLevelReached);
	}
	private void getThreads(final double longitude, final double latitude, final ThreadsCallback callback, final int level){
		Log.i(this.getClass().getName(),"getThreads, level:"+level);
		highestLevelReached = Math.max(highestLevelReached, level);
		int newLongitude = getNewLatLong(longitude);
		int newLatitude = getNewLatLong(latitude);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Thread");
		
		int finalPullRadius = (int) (PULL_RADIUS * Math.pow(LEVEL_MULTIPLIER, level-1));
		query.whereGreaterThanOrEqualTo("longitude", newLongitude - finalPullRadius);
		query.whereLessThanOrEqualTo("longitude", newLongitude + finalPullRadius);
		query.whereGreaterThanOrEqualTo("latitude", newLatitude - finalPullRadius);
		query.whereLessThanOrEqualTo("latitude", newLatitude + finalPullRadius);

		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objectList, ParseException e) {
				if (e == null) {
					ArrayList<Thread> threadList = new ArrayList<Thread>();
					for(ParseObject object : objectList){
						String threadID = object.getObjectId();
						String threadTitle = object.getString("threadTitle");
						Log.i("get threads","parseobject:"+threadID+":"+threadTitle);
						String firstCommentContent = object.getString("firstCommentContent");
						String ownerID = object.getString("ownerID");
						ArrayList<Comment> commentList = new ArrayList<Comment>(); //No comments for multi requests
						Thread thread = new Thread(threadID,threadTitle,firstCommentContent,ownerID,commentList);

						thread.dateCreated = object.getCreatedAt();
						thread.dateUpdated = object.getUpdatedAt();
						threadList.add(thread);
					}
					if(threadList.isEmpty() && level + 1 <= MAX_LEVEL_FOR_DYNAMIC_THREADS){
						//RECURSION
						getThreads(longitude, latitude, callback, level + 1);
					}else{
						moderateLevelReached = Math.max(1, level - 1);
						callback.success(threadList);						
					}
				} else {
					callback.failure(e.getMessage());
				}
			}
		});

	}

	private void getThread(String threadID, final ParseObjectCallback callback){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Thread");
		query.getInBackground(threadID, new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					callback.success(object);
				} else {
					callback.failure(e.toString());
				}
			}
		});
	}

	private static Thread threadFromParseObject(ParseObject object){
		String threadID = object.getObjectId();
		String threadTitle = object.getString("threadTitle");
		String firstCommentContent = object.getString("firstCommentContent");
		String ownerID = object.getString("ownerID");
		ArrayList<Comment> commentList = new ArrayList<Comment>();
		return new Thread(threadID,threadTitle,firstCommentContent,ownerID,commentList);
	}

	public void getThread(String threadID, final ThreadCallback callback){
		Log.i(this.getClass().getName(),"getThread");
		getThread(threadID, new ParseObjectCallback(){
			@Override
			public void success(ParseObject object) {
				callback.success(threadFromParseObject(object));
			}
			@Override
			public void failure(String error) {
				callback.failure("Get Thread Error: "+error);
			}
		});
	}

	public void getFollowingThreads(final ThreadsCallback callback){
		Log.i(this.getClass().getName(),"getFollowingThreads");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("AnonUser");
		query.whereEqualTo("phoneID", phoneID);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> userList, ParseException e) {
				if (e == null) {
					if(userList.size()!=0){
						ParseObject user = userList.get(0);
						List<String> threadIDs = user.getList("followedthreads");
						//if(threadIDs != null) Log.i("getfollowingthreads","threadIDs size:"+threadIDs.size());
						if(threadIDs != null && threadIDs.size()>0){
							ArrayList<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
							for(String threadID : threadIDs){
								//Log.i("getfollowingthreads","threadID:"+threadID);
								ParseQuery<ParseObject> q = ParseQuery.getQuery("Thread");
								q.whereEqualTo("objectId",threadID);
								queries.add(q);			    				
							}
							ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
							mainQuery.findInBackground(new FindCallback<ParseObject>(){
								@Override
								public void done(List<ParseObject> objects,
										ParseException e) {
									//Log.i("getfollowingthreads","multi query size:"+objects.size());
									ArrayList<Thread> threads = new ArrayList<Thread>();
									for(ParseObject o : objects){
										Thread newThread = threadFromParseObject(o);
										newThread.dateCreated = o.getCreatedAt();
										newThread.dateUpdated = o.getUpdatedAt();
										threads.add(newThread);
									}
									callback.success(threads);
								}
							});
						}else{
							callback.success(new ArrayList<Thread>());			    			
						}
					}else{
						callback.success(new ArrayList<Thread>());
					}
				}else{
					callback.failure("Error in getting followed threads 1:"+e.toString());
				}
			}
		});
	}

	public void createThread(final double longitude, final double latitude, final String threadTitle, String threadFirstComment, final ServerCallback callback){
		Log.i(this.getClass().getName(),"createThread, location:"+longitude+","+latitude);
		final ParseObject threadObj = new ParseObject("Thread");
		int newLongitude = getNewLatLong(longitude);
		int newLatitude = getNewLatLong(latitude);
		//final String threadLocationChannel = "location"+newLongitude/SUB_LOCATION_RADIUS+"_"+newLatitude/SUB_LOCATION_RADIUS;
		ArrayList<String> tempThreadSurroundLocationChannels = new ArrayList<String>();
		boolean tempIsThreadInArea =false;
		for(int i =-1;i<=1;i++){
			for(int j =-1;j<=1;j++){
				String surroundString = "location"+(newLongitude/SUB_LOCATION_RADIUS + i)+"_"+(newLatitude/SUB_LOCATION_RADIUS + j);
				tempThreadSurroundLocationChannels.add(surroundString);
				if(surroundString.equals(locationChannel)) tempIsThreadInArea = true;
			}
		}
		final ArrayList<String> threadSurroundLocationChannels = tempThreadSurroundLocationChannels;
		final boolean isThreadInArea = tempIsThreadInArea;
		final String locationChannelAtTimeOfPost = locationChannel;
		if(isThreadInArea){
			isPushingLocation = true;
			PushService.unsubscribe(context, locationChannelAtTimeOfPost);
		}
		
		threadObj.put("longitude", newLongitude);
		threadObj.put("latitude", getNewLatLong(latitude));
		threadObj.put("ownerID", phoneID);
		threadObj.put("threadTitle", threadTitle);
		threadObj.put("firstCommentContent", threadFirstComment);
		threadObj.put("nextAvailableNameIndex", 1);
		threadObj.put("numberOfComments", 0);
		threadObj.put("Comments", new ArrayList<String>());

		threadObj.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e == null) {
					String threadID = threadObj.getObjectId();
					getInstance().followThread(threadID, callback); // auto follow

					ParsePush pushForLocation = new ParsePush();
					pushForLocation.setChannels(threadSurroundLocationChannels);
					pushForLocation.setMessage("Someone just posted a new thread in your area! \""+threadTitle+"\"");
					
					pushForLocation.sendInBackground(new SendCallback(){
						@Override
						public void done(ParseException e) {
							if(e==null){
								new Timer().schedule(new TimerTask(){
									@Override
									public void run() {
										if(isThreadInArea){
											isPushingLocation = false;
											if(locationChannelAtTimeOfPost.equals(locationChannel)){
												Log.i("Push","ReSub after pushForLocation");
												if(!isApplicationClosed){
													PushService.subscribe(context, locationChannelAtTimeOfPost,activity);													
												}
											}else{
												Log.i("Push","location changed after pushing");
											}
										}
									}
								}, 3000);
								Log.i("Push", "Create New Thread Push Success");
							}else{
								Log.e("Push", "Create New Thread Push failed "+e.getMessage());
							}
						}
					});
					
					
				} else {
					callback.failure("Error in creating: "+e.toString());
				}
			}
		});
	}

	public void getComments(final String threadID, final CommentsCallback callback){
		Log.i(this.getClass().getName(),"getComments");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Comment");
		query.whereEqualTo("threadID", threadID);
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if(e==null){
					ArrayList<Comment> comments = new ArrayList<Comment>();
					for(ParseObject o : objects){
						Comment c = new Comment(o.getObjectId(),
								o.getInt("nameIndex"),
								o.getString("content"),
								threadID,
								o.getString("phoneID"));
						comments.add(c);
					}
					Log.i("GetComments","commentList size:"+comments.size());
					callback.success(comments);
				}else{
					callback.failure("GetComments Error: "+e.getMessage());
				}
			}
		});
	}

	public void commentOnThread(final String threadID, final String commentContent, final ServerCallback callback){
		Log.i(this.getClass().getName(),"commentOnThread");
		getThread(threadID, new ParseObjectCallback(){
			@Override
			public void failure(String error) {
				callback.failure("Comment on thread Error 1: "+error);
			}
			@Override
			public void success(final ParseObject threadObject){
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Comment");
				query.whereEqualTo("threadID", threadID);
				query.whereEqualTo("phoneID", phoneID);
				query.findInBackground(new FindCallback<ParseObject>(){
					@Override
					public void done(List<ParseObject> commentObjects, ParseException e) {
						if(e!=null){
							callback.failure("Comment on thread error 2:"+e.getMessage());
						}else{
							int nextIndex = threadObject.getInt("nextAvailableNameIndex");
							if(threadObject.getString("ownerID").compareTo(phoneID)==0){
								nextIndex = 0;
							}else if(commentObjects.size()==0){
								threadObject.increment("nextAvailableNameIndex");
							}else{
								ParseObject commentObj = commentObjects.get(0);
								nextIndex = commentObj.getInt("nameIndex");
							}
							threadObject.increment("numberOfComments");
							threadObject.saveInBackground();

							ParseObject newCommentObj = ParseObject.create("Comment");
							newCommentObj.put("nameIndex", nextIndex);
							newCommentObj.put("content",commentContent);
							newCommentObj.put("threadID", threadID);
							newCommentObj.put("phoneID", phoneID);
							newCommentObj.saveInBackground(new SaveCallback(){
								@Override
								public void done(ParseException e) {
									if(e==null){
										callback.success();
										ArrayList<Thread> threads = Manager.getInstance().followedArray;
										boolean isFollowingTemp = false;
										final Thread currentThread = Manager.getInstance().currentThread;
										for(Thread t : threads){
											if(t.threadID.compareTo(threadID)==0){
												isFollowingTemp=true;
												break;
											}
										}
										final boolean isFollowing = isFollowingTemp;

										Thread tempThread;
										tempThread = Manager.getInstance().getThreadWithID(threadID);
										if(tempThread==null){
											tempThread = Manager.getInstance().getFollowingThreadWithID(threadID);
										}
										if(isFollowing){
											PushService.unsubscribe(context, "thread"+threadID);
										}
										new Timer().schedule(new TimerTask(){
											@Override
											public void run() {
												ParsePush pushForFollowers = new ParsePush();
												pushForFollowers.setChannel("thread"+threadID);
												if(currentThread!=null){
													pushForFollowers.setMessage("Someone just commented on "+currentThread.threadTitle+".");
												}else{
													pushForFollowers.setMessage("Someone just commented on @nonymous.");
												}
												
												pushForFollowers.sendInBackground(new SendCallback(){
													@Override
													public void done(ParseException e) {
														if (e == null){
															Log.i("Push","Push to followers success");
														}else{
															Log.e("Push","Push to followers failed "+e.getMessage());
														}
														if(isFollowing){
															new Timer().schedule(new TimerTask(){
																@Override
																public void run() {
																	Log.i("Push","Resubing to followed thread");
																	PushService.subscribe(context, "thread"+threadID,activity);
																}
															},3000);
														}
													}
												});
											}
										},3000);
									}else{
										callback.failure("Comment on thread error 3: "+e.getMessage());
									}	
								}
							});
						}
					}
				});
			}
		});
	}

	public void followThread(final String threadID, final ServerCallback callback){
		Log.i(this.getClass().getName(),"followThread");
		//Server should auto followThread if person creates Thread
		ParseQuery<ParseObject> query = ParseQuery.getQuery("AnonUser");
		Log.i("server.follow","phoneID:"+phoneID);
		query.whereEqualTo("phoneID", phoneID);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> userList, ParseException e) {
				if (e == null) {
					ParseObject user;
					Log.i("Server.follow", "userlist size:"+userList.size());
					if(userList.size()==0){
						user = new ParseObject("AnonUser");
						user.put("phoneID", phoneID);
					}else{
						user = userList.get(0);
						if(userList.size()>1) Log.e("Server.follow","Error, userlist size >1 : "+userList.size());
					}

					Log.i("Server.follow"," user : "+user.getObjectId());

					user.put("following"+threadID, "yes");
					user.addUnique("followedthreads", threadID);
					PushService.subscribe(context, "thread"+threadID, activity);

					user.saveInBackground(new SaveCallback(){
						@Override
						public void done(ParseException e2) {
							if(e2==null){
								callback.success();								
							}else{
								Log.e("Server.followThread","Error in following p2: "+e2.toString());
								callback.failure("Error in following p2: "+e2.toString());
							}
						}
					});
				} else {
					Log.e("Server.followThread","Error in following p1:"+e.toString());
					callback.failure("Error in following p1:"+e.toString());			    		
				}
			}
		});
	}

	public void unFollowThread(final String threadID, final ServerCallback callback){
		Log.i(this.getClass().getName(),"unfollowThread");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("AnonUser");
		query.whereEqualTo("phoneID", phoneID);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> userList, ParseException e) {
				if (e == null) {
					if(userList.size()==0){
						callback.success();
					}else{
						ParseObject user = userList.get(0);
						user.put("following"+threadID, "no");
						ArrayList<String> arr = new ArrayList<String>();
						arr.add(threadID);
						user.removeAll("followedthreads",arr);

						user.saveInBackground(new SaveCallback(){
							@Override
							public void done(ParseException e2) {
								if(e2==null){
									callback.success();
									PushService.unsubscribe(context, "thread"+threadID);
								}else{
									Log.e(this.getClass().getName(),"Error in unfollowing p2: "+e2.toString());
									callback.failure("Error in unfollowing p2: "+e2.toString());
								}
							}
						});
					}
				} else {
					callback.failure("Error in unfollowing p1: "+e.toString());	
					Log.e(this.getClass().getName(),"Error in unfollowing p1: "+e.toString());			    		
				}
			}
		});
	}

	private static int getNewLatLong(double latOrLong){
		return (int) Math.round( latOrLong / LAT_LONG_THRESHOLD);
	}
	public void applicationClose(){
		isApplicationClosed = true;
		unSubLocation();
	}
	private void unSubLocation() {
		if(locationChannel != null && !isPushingLocation){
			PushService.unsubscribe(context, locationChannel);
		}
		locationChannel = null;
	}
	private void subLocation(double longitude,double latitude){
		isApplicationClosed = false;
		
		int newLongitude = getNewLatLong(longitude);
		int newLatitude = getNewLatLong(latitude);
		final String newLocationChannel = "location"+newLongitude/SUB_LOCATION_RADIUS+"_"+newLatitude/SUB_LOCATION_RADIUS;
		if(newLocationChannel.equals(locationChannel)) return;

		unSubLocation();
		new Timer().schedule(new TimerTask(){
			@Override
			public void run() {
				if(!isApplicationClosed){
					PushService.subscribe(context, newLocationChannel, activity);
					locationChannel = newLocationChannel;					
				}
			}			
		}, 3000);
	}
}
