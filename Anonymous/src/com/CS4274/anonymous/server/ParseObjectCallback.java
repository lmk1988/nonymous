package com.CS4274.anonymous.server;

import com.parse.ParseObject;

public interface ParseObjectCallback {
	void success(ParseObject object);
	void failure(String error);
}
