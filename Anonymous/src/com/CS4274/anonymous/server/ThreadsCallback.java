package com.CS4274.anonymous.server;

import java.util.ArrayList;
import com.CS4274.anonymous.Thread;

public interface ThreadsCallback {
	void success(ArrayList<Thread> thread);
	void failure(String error);
}
