package com.CS4274.anonymous.server;

public interface ServerCallback {
	void success();
	void failure(String error);
}
