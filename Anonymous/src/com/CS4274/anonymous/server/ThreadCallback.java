package com.CS4274.anonymous.server;

import com.CS4274.anonymous.Thread;

public interface ThreadCallback {
	void success(Thread thread);
	void failure(String error);
}
