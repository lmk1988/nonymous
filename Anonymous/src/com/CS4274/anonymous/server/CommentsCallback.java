package com.CS4274.anonymous.server;

import java.util.ArrayList;
import com.CS4274.anonymous.Comment;


public interface CommentsCallback {
	void success(ArrayList<Comment> comments);
	void failure(String error);
}
