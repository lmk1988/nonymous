package com.CS4274.anonymous;

import java.util.ArrayList;

import com.CS4274.anonymous.server.ThreadsCallback;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class LocationDelegate implements LocationListener{
	@Override
	public void onLocationChanged(Location location) {
        Log.i(this.getClass().getName(), "onLocationChanged");
        Manager.getInstance().latestLongitude = location.getLongitude();
		Manager.getInstance().latestLatitude = location.getLatitude();
        
        //Only pull from server if there was a location change more than the location threshold
        if((Manager.getInstance().latestLongitude==0 && Manager.getInstance().latestLatitude==0) || 
        		getDistance(Manager.getInstance().syncLongitude,Manager.getInstance().syncLatitude,Manager.getInstance().latestLongitude,Manager.getInstance().latestLatitude)>0.5){     
	  
        	//Update here and not inside the callback, don't want to keep spamming if it fails or succeed
        	Manager.getInstance().syncLongitude = location.getLongitude();
        	Manager.getInstance().syncLatitude = location.getLatitude();
        	
        	Server.getInstance().getThreads(location.getLongitude(), location.getLatitude(),new ThreadsCallback(){
				@Override
				public void success(ArrayList<Thread> thread) {
					Log.i(this.getClass().getName(),"updateThreads");
					Manager.getInstance().updateThreads(thread);
				}
	
				@Override
				public void failure(String error) {
					Log.e(this.getClass().getName(),"Unable to getThreads from Server");
				}
	        });
        }else{
        	Log.i(this.getClass().getName(), "Location too close to previous location, did not pull from Server");
        }
	}
	@Override
	public void onProviderDisabled(String provider) {
		Log.i(this.getClass().getName(), "onProviderDisabled: "+provider);
		if(provider.compareToIgnoreCase(LocationManager.GPS_PROVIDER)==0){
			Manager.getInstance().bol_GPSLocationEnabled=false;
		}else if(provider.compareToIgnoreCase(LocationManager.NETWORK_PROVIDER)==0){
			Manager.getInstance().bol_WIFILocationEnabled=false;
		}
	}
	@Override
	public void onProviderEnabled(String provider) {
		Log.i(this.getClass().getName(), "onProviderEnabled: "+provider);
		if(provider.compareToIgnoreCase(LocationManager.GPS_PROVIDER)==0){
			Manager.getInstance().bol_GPSLocationEnabled=true;
		}else if(provider.compareToIgnoreCase(LocationManager.NETWORK_PROVIDER)==0){
			Manager.getInstance().bol_WIFILocationEnabled=true;
		}
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.i(this.getClass().getName(), "onStatusChanged");
	}
	
	private double getDistance(double long1,double lat1, double long2, double lat2){
		double dlon = long2 - long1;
		double dlat = lat2 - lat1;
		double a = Math.pow((Math.sin(dlat/2)),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow((Math.sin(dlon/2)),2);
		double c = 2 * Math.atan2( Math.sqrt(a), Math.sqrt(1-a) );
		double R= 6373; //(where R is the radius of the Earth) 
		return R * c; 
	}
}
