package com.CS4274.anonymous;

public interface CommentObserver {
	public void pullFromManager();
}
