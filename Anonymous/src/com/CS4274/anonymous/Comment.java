package com.CS4274.anonymous;

public class Comment {
	String commentId;
	int commenterIndex;
	String content;
	String threadId;
	String phoneID;
	
	public Comment(String commentId, int commenterIndex, String content, String threadId, String phoneID){
		this.commentId = commentId; //object id
		this.commenterIndex = commenterIndex; //name index
		this.content = content; //context
		this.threadId = threadId; //thead id
		this.phoneID = phoneID; //thead id
	}
}
