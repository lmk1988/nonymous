package com.CS4274.anonymous;

import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.content.Context;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.parse.ParseAnalytics;
import com.parse.PushService;

public class MainActivity extends SherlockFragmentActivity {
	

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Server.getInstance().applicationClose();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Manager.getInstance().mainContext = this;
		
		setTitle("@nonymous");
		
		if(Manager.getInstance().locationManager==null){
			//Set phoneID
			Manager.getInstance().phoneID = Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
			Log.v("phoneID",Manager.getInstance().phoneID);
			
			//Set locationManager
			Manager.getInstance().locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if(Manager.getInstance().locationManager!=null){
				Manager.getInstance().initLocationProviders();
			}else{
				Log.e(this.getClass().getName(),"locationManager could not be created");
			}
		}
 		
 		//Auto start before user moves to that tab
 		Manager.getInstance().getFollowingThreadsFromServer();
 		
 		//Register for push.
 		ParseAnalytics.trackAppOpened(getIntent());
 		Server.registerDevice(Manager.getInstance().mainContext, MainActivity.class);
		PushService.setDefaultPushCallback(Manager.getInstance().mainContext, MainActivity.class);


		//Set view of activity
		ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        actionBar.setDisplayShowTitleEnabled(true);
        
        
        Tab tab = actionBar.newTab()
        					.setText("Topics Around You")
        					.setTabListener(new FirstTabFragment());
        					//.setIcon(R.drawable.android);
        
        actionBar.addTab(tab);
        
        tab = actionBar.newTab()
							.setText("Topics Followed")
							.setTabListener(new SecondTabFragment());
							//.setIcon(R.drawable.apple);
        
        actionBar.addTab(tab);
        
	}
	
	
}
