package com.CS4274.anonymous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class Thread {
	String threadID;
	String threadTitle;
	String firstCommentContent;
	String ownerID; //phoneid of owner
	int ownerNameIndex = 0;
	int MAX_COMMENTS = 500;
	ArrayList<String> nameList;
	ArrayList<Comment> commentList;
	Date dateCreated;
	Date dateUpdated;
	
	private static List<String> initialNameList = Arrays.asList("Abel Gilbert", "Terry Hopkins", "Ella Farmer", "Stephen Dixon", "Ray Miller", "Virgil Dunn", "Jamie Conner", "Elvira Cunningham", "Matthew Lloyd", "Leah Mitchell", "Mitchell Lawrence", "Evan Copeland", "Kristine Townsend", "Jerome Clayton", "Brandon Klein", "Horace Reynolds", "Maxine Long", "Essie Palmer", "Doris Pope", "Traci Burke", "Ruby Franklin", "Sammy Robbins", "Douglas Daniel", "Darrell Higgins", "Kerry Nguyen", "Felicia Casey", "Gregory Rodgers", "Rita Mclaughlin", "Hugo Baldwin", "Sonya Morgan", "Ana Thomas", "Laverne Bradley", "Dominic Newton", "Ronald Kim", "Kyle Tate", "Genevieve Lopez", "Megan Carlson", "Kenny Ballard", "Carlton Erickson", "Ben Rivera", "Jeanette Buchanan", "Stacey Wong", "Louis Holland", "Joanne Fitzgerald", "Claude Carson", "Victoria Sullivan", "Ellis Ellis", "Wilbert Hoffman", "Misty Dennis", "Sophia Fernandez", "Jill Ingram", "Michael Hansen", "Oscar Howard", "Meghan Sims", "Carroll Harper", "Leslie Silva", "Lee Kelley", "Bridget Webster", "Aaron Floyd", "Alyssa Gill", "Pat Cortez", "Cheryl Campbell", "Kathy Phillips", "Brenda Moreno", "Nettie Freeman", "Seth Montgomery", "Max Wise", "Rolando Sanders", "Salvatore Martin", "Flora Strickland", "Abraham Knight", "Angelica Schwartz", "Verna Benson", "Ada Gross", "Harry Ramos", "Christian Cooper", "Delbert Rhodes", "Bessie Welch", "Gertrude Andrews", "Bernice Wolfe", "Gina Peters", "Samantha Hernandez", "Walter Grant", "Jackie Anderson", "Isabel Foster", "Kimberly Crawford", "Linda Burgess", "Glenda Ferguson", "Charlie Wright", "Eloise Barton", "Elena Garza", "Lindsey Barnes", "Ivan Craig", "Susan Dawson", "Mark Todd", "Kathleen Cummings", "Donald Sharp", "Joanna Olson", "Penny Garrett", "Forrest Hammond", "Kristopher Greene", "Jessie Gomez", "Antoinette Clark", "Gerardo Tyler", "Colin Cain", "Maria Boyd", "Veronica Cannon", "Lynne Steele", "Theresa Parker", "Lee Richards", "Hilda Norman", "Jason Goodman", "Roberto Rios", "Jared Wallace", "Lauren Manning", "Emmett Snyder", "Carolyn Cruz", "Rose Wade", "Rufus Simon", "Marlene Matthews", "Jeff Fisher", "Roland Martinez", "Ian Saunders", "Tom Hampton", "Cecilia Myers", "Willie Aguilar", "Pedro Delgado", "Miranda Boone", "Kara Meyer", "Paul Mcgee", "Dwayne Haynes", "Doyle Byrd", "Irvin Duncan", "Jimmie Lane", "Julia Dean", "Otis Poole", "Shelly Pearson", "Muriel Newman", "Whitney Chapman", "Margaret Reese", "Leona Moss", "Andres Gray", "Curtis Curry", "Tabitha Santiago", "Eleanor Allison", "Nicholas Lee", "Jerald Marsh", "Antonio Ramsey", "Tommy French", "Sally Gibson", "Joe Vargas", "Jennifer Walton", "Hazel Russell", "Eric Bates", "Luis Simmons", "Tara James", "Jermaine Sutton", "Lester Green", "Delia Fuller", "Conrad Graves", "Kerry Sparks", "Domingo Harrington", "Noah Neal", "Tiffany Jimenez", "Henrietta Gonzalez", "Elisa Patterson", "Anne Hart", "Priscilla Murray", "Geoffrey Mckinney", "Leroy Morales", "Mamie Davidson", "Gabriel Gutierrez", "Rachael Shelton", "Derek Carroll", "Russell Thompson", "Jeffery Osborne", "Sherri Munoz", "Danny Simpson", "Cory Caldwell", "Chester Mann", "Carmen Patrick", "Leticia Roberson", "Yvette Jordan", "James Mason", "Allison Tucker", "Randall Medina", "Jody Thornton", "Jimmy Hudson", "Georgia Webb", "Jacquelyn Bailey", "George Harris", "Erika Fields", "Elaine Hanson", "Jesus Massey", "Dawn Hale", "Van Hicks", "Josefina Pena", "Brent Collier", "Helen Abbott", "Steve Curtis", "Rodolfo Mcbride", "Maryann Adkins", "Ellen Herrera", "Mandy Coleman", "Al Guerrero", "Derrick Mcdonald", "Celia Roberts", "Gordon West", "Clayton Stevenson", "Judith Rowe", "Roosevelt Burton", "Elias Henderson", "Beatrice Little", "Renee Jensen", "Vernon Fletcher", "Diane Maldonado", "Elsa Mcdaniel", "Orville Mccormick", "Dewey Lynch", "Sharon Underwood", "Adam Moran", "Marian Morrison", "Nicole Rose", "Josh Brewer", "Michelle Christensen", "Gretchen Arnold", "Thomas Smith", "Ramona Hawkins", "Joan Richardson", "Clinton Mendez", "Corey Garcia", "Sidney Quinn", "Christie Brown", "Yolanda Bishop", "Kristi Singleton", "Kent Turner", "Roderick Schultz", "Manuel Collins", "Desiree Vaughn", "Wilma Rice", "Terence Valdez", "Greg Zimmerman", "Tami Mullins", "Erica Lambert", "Patti Vega", "Vera Gordon", "Kayla Gardner", "Peter Willis", "Marc Moody", "Terri Santos", "Taylor Jennings", "Cecelia Ford", "Christine Elliott", "Luz Bowen", "Olivia Watkins", "Lionel Armstrong", "Trevor Sandoval", "Vanessa Brooks", "Miriam Chavez", "Rene Parks", "Donna Frank", "Louise Diaz", "Tomas Mills", "Sonia Griffith", "Clifton Henry", "Nadine Bennett", "Bernard Kelly", "Ernestine Ryan", "Anna Francis", "Joel Swanson", "Dianna Nelson", "Lowell Ortiz", "Rickey Wells", "Vivian Nichols", "Rebecca Walker", "Jaime Brady", "Carole Daniels", "Sheila Carr", "Charles Walsh", "Pam Mathis", "Myra Becker", "Glen Riley", "Heidi Hayes", "Guadalupe Love", "Lorena Wagner", "Theodore Douglas", "Clifford Huff", "Reginald Taylor", "Mattie Yates", "Jenny Torres", "Ruth Holmes", "Della Hughes", "June Weber", "Rochelle Harvey", "Frances Holloway", "Carrie Ross", "Lyle Jefferson", "Dana Paul", "Oliver Cole", "Ervin Drake", "Earnest Walters", "William Mendoza", "Ron Kennedy", "Marshall Brock", "Rosemarie Peterson", "Bernadette Reed", "Cynthia Young", "Jorge Bryant", "Eva Guzman", "Lena George", "John Patton", "Carl Mccoy", "Laurence Fleming", "Cassandra Alexander", "Sheldon Chambers", "Santiago Schmidt", "Paulette Owen", "Lila Allen", "Rosie Phelps", "Elbert Lindsey", "Alexander Owens", "Gladys Ramirez", "Darin Bass", "Antonia Reeves", "Deborah Terry", "Martha Rodriguez", "Guillermo Page", "Pat Lewis", "Maurice Flowers", "Peggy Reyes", "Guadalupe Barber", "Andrea Williams", "Casey Murphy", "Duane Gregory", "Tasha Leonard", "Daisy Horton", "Amos Warner", "Nelson Tran", "Franklin Holt", "Angel Miles", "Marilyn Williamson", "Shannon Davis", "Toby Stewart", "Lamar Sherman", "Angie Hodges", "Eduardo Perkins", "Lorenzo Mack", "Regina Cobb", "Lynn Robertson", "Jan Stephens", "Johnnie Griffin", "Jeannie Castillo", "Dolores Watson", "Lloyd Maxwell", "Caleb Evans", "Raul Baker", "Brandy Beck", "Saul Stevens", "Julie Harrison", "Blake Blair", "Estelle Goodwin", "Amber Howell", "Shaun Alvarez", "Irma Morris", "Jordan Fowler", "Naomi Nash", "Tommie Wilkins", "David Graham", "Josephine Watts", "Marlon Carter", "Wesley Doyle", "Ricardo Adams", "Wayne Harmon", "Clay Keller", "Audrey Park", "Roman Glover", "Edwin Lucas", "Alfonso Bowman", "Phillip Houston", "Bryant Nunez", "Holly Garner", "Tracy Padilla", "Arlene Rogers", "Belinda Wood", "Fannie Potter", "Wilfred Chandler", "Beverly Pierce", "Amanda Stone", "Melinda Malone", "Noel Roy", "Jessica Fox", "Agnes Ward", "Charlotte Pittman", "Elmer Clarke", "Damon Barnett", "Dana Cook", "Marjorie Mckenzie", "Sean Robinson", "Bobbie Luna", "Suzanne Schneider", "Bob Cross", "Lillian Pratt", "Patsy Barrett", "Gilberto Oliver", "Eunice Johnston", "Sam Lyons", "Micheal Ray", "Dominick Johnson", "Blanche Bryan", "Gwen Barker", "Eileen Gonzales", "Bobby Shaw", "Sara Butler", "Kirk Blake", "Lindsay Figueroa", "Anthony Hogan", "Randy Alvarado", "Mario Porter", "Katherine Rodriquez", "Ora Jackson", "Karen Hill", "Alice Joseph", "Maggie Jenkins", "Susie Mcguire", "Marty Mccarthy", "Mable Logan", "Timothy Marshall", "Latoya Salazar", "Jodi Greer", "Ken Woods", "Aubrey Hamilton", "Archie Hall", "Patty Soto", "Dale Briggs", "Clint Hubbard", "Faith Ortega", "Meredith Larson", "Kelly Lamb", "Blanca Obrien", "Leigh Stanley", "Michele Vasquez", "Carol Summers", "Gwendolyn Powers", "Sergio Wheeler", "Courtney Cohen", "Mary Estrada", "Javier Austin", "Kim Waters", "Wade Hardy", "Margie Perry", "Darryl Colon", "Rosa Jones", "Alvin Cox", "Barbara Burns", "Jody Perez", "Merle Ruiz", "Jon Berry", "Tammy Moore", "Jan Lawson", "Victor Carpenter", "Evelyn Black", "Juana Hines", "Moses Edwards", "Dwight Wilkerson", "Roberta King", "Phyllis Parsons", "Guy Bowers", "Melvin Banks", "Emanuel Washington", "Stephanie May", "Juanita Frazier", "Melanie Morton", "Bryan Lowe", "Rachel Flores", "Matt Romero", "Alma Powell", "Alan Spencer", "Dorothy Norton", "Felipe Gibbs", "Donnie Norris", "Katrina Sanchez", "Kurt Bridges", "Mack Warren", "Zachary Scott", "Mae Bell", "Annie Payne", "Spencer Day", "Bertha Jacobs", "Alex Bush", "Sue Ball", "Francis Price", "Debra White", "Billie Hunter", "Shane Weaver", "Jean Wilson", "Ted Castro", "Byron Stokes", "Laura Reid", "Marta Hunt", "Julius");
	
	//Followers, commenter, owner id should be stored in server
	public Thread(String threadID, String threadTitle, String firstCommentContent, String ownerID, ArrayList<Comment> commentList){
		// Existing Thread
		
		this.threadID = threadID;
		this.firstCommentContent = firstCommentContent;
		this.threadTitle = threadTitle;
		this.ownerID = ownerID;
		this.nameList = getNameListArray(threadID);
		this.commentList = commentList;
	}
	public Thread(String threadID, String threadTitle, String firstCommentContent){
		// New Thread
		
		this.threadID = threadID;
		this.firstCommentContent = firstCommentContent;
		this.threadTitle = threadTitle;
		this.ownerID = Manager.getInstance().phoneID;
		this.nameList = getNameListArray(threadID);
		this.commentList = new ArrayList<Comment>();
	}
	
	public static ArrayList<String> getNameListArray(String seed){
		String[] input = (String[]) initialNameList.toArray();
		String[] shuffled = shuffleArray(input,seed);
		ArrayList<String> output = new ArrayList<String>();
		for(int i=0;i<shuffled.length;i++){
			output.add(shuffled[i]);
		}
		return output;
	}
		
	private static String[] shuffleArray(String[] array, String seed){
		if(seed == null) seed = "0";
		
		String[] output = new String[array.length];
		System.arraycopy(array, 0, output, 0, array.length);
		
		Random rnd = new Random(Long.parseLong(seed, 36));
		for (int i = output.length - 1; i > 0; i--){
			int index = rnd.nextInt(i + 1);
			// Simple swap
			String a = output[index];
			output[index] = output[i];
			output[i] = a;
		}
		return output;
	}
	
	@Override
	public String toString(){
		//For view
		return threadTitle;
	}
}
