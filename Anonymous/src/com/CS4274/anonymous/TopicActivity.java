package com.CS4274.anonymous;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TopicActivity extends Activity implements CommentObserver{
	MenuItem followButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Thread");
		resetView();
	}
	
	private void addComment(String commenterName, String comment){
		LinearLayout layout = (LinearLayout) findViewById(R.id.layout_comments);
		LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		TextView commentView =new TextView(this);
		commentView.setLayoutParams(lparams);
		
		//Set color of text
		SpannableString ss = new SpannableString(commenterName+": "+comment);
		ss.setSpan(new RelativeSizeSpan(1.5f), 0, commenterName.length(), 0);
		ss.setSpan(new RelativeSizeSpan(1.3f), commenterName.length(), ss.length(), 0);
		ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, commenterName.length(), 0);
		ss.setSpan(new ForegroundColorSpan(Color.DKGRAY), commenterName.length(), ss.length(), 0);
		
		commentView.setText(ss);
		
		int indexToPlace = layout.indexOfChild(findViewById(R.id.edittext));
		
		layout.addView(commentView,indexToPlace);
		
		//Add gray line
		View line = new View(this);
		line.setBackgroundColor(Color.DKGRAY);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 2);
		lp.setMargins(4, 4, 4, 4);
		line.setLayoutParams(lp);
		layout.addView(line,indexToPlace+1);
	}
	
	private void resetView(){
		Log.i("Topic","resetView");
		setContentView(R.layout.activity_main);
		final TextView topic;
		final EditText commentBox;
		final Button post;
		Intent intent = getIntent();
		
		//Title
		topic = (TextView) findViewById(R.id.topic);
		
		int index = intent.getExtras().getInt("index");
		Thread currentThread;
		if(intent.getExtras().getString("arrayType").compareToIgnoreCase("threadArray")==0){
			currentThread = Manager.getInstance().threadArray.get(index);
		}else if(intent.getExtras().getString("arrayType").compareToIgnoreCase("followedArray")==0){
			currentThread = Manager.getInstance().followedArray.get(index);
		}else{
			Log.e("Topic resetview","currentThread is gone");
			currentThread = null;
			return;
		}
		
		Manager.getInstance().currentThread = currentThread;
		
		SpannableString ss = new SpannableString(currentThread.threadTitle);
		ss.setSpan(new RelativeSizeSpan(2f), 0, ss.length(), 0); // set size
		ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ss.length(), 0);// set color
		topic.setText(ss);
		
		//First comment by thread user
		if(Manager.getInstance().currentThread.ownerID.compareTo(Manager.getInstance().phoneID)==0){
			addComment(currentThread.nameList.get(0)+"(You)",currentThread.firstCommentContent);
		}else{
			addComment(currentThread.nameList.get(0),currentThread.firstCommentContent);
		}
		
		commentBox = (EditText) findViewById(R.id.edittext);
		post = (Button) findViewById(R.id.button);
		
		post.setOnClickListener(new OnClickListener() {
			public void onClick(View v){
				//Check for Internet and location before proceeding
				if(!Manager.getInstance().checkConnectionAndLocationBeforeProceeding(TopicActivity.this)){
					return;
				}
				
				commentBox.setText(commentBox.getText().toString().trim());
				if(commentBox.getText().toString().length()==0){
					Toast toast = Toast.makeText(TopicActivity.this, "Comment cannot be empty", Toast.LENGTH_SHORT);
            		toast.show();
            		return;
				}
				
				Manager.getInstance().commentOnThread(Manager.getInstance().currentThread.threadID, commentBox.getText().toString());
				//No idea which id is the phone itself
				addComment("You",commentBox.getText().toString());
				commentBox.setText("");
				Toast toast = Toast.makeText(TopicActivity.this, "Pushing Comments To Server...", Toast.LENGTH_SHORT);
				toast.show();
			}
		});
		
		//populate comments based on manager
		ArrayList<Comment> commentArray = currentThread.commentList;
		for(int i=0;i<commentArray.size();i++){
			if(commentArray.get(i).phoneID.compareTo(Manager.getInstance().phoneID)==0){
				addComment(currentThread.nameList.get(commentArray.get(i).commenterIndex)+"(You)",commentArray.get(i).content);
			}else{
				addComment(currentThread.nameList.get(commentArray.get(i).commenterIndex),commentArray.get(i).content);
			}
		}
		
		Manager.getInstance().setCommentObserver(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		Log.i("Topic","oncreateoptionsmenu");
		boolean userFollows = false;
		if(Manager.getInstance().getFollowingThreadWithID(Manager.getInstance().currentThread.threadID)!=null){
			//Have to do this because if we search contains, it might not be the same object but have the same threadID
			userFollows = true;
		}
		
		int groupID = 1;
		//Remove any previous icons
		menu.removeGroup(groupID);
		
		if(userFollows==false){
			followButton = menu.add(groupID, 0, 0, "Follow");
			followButton.setIcon(R.drawable.empty_star).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}else{
			followButton = menu.add(groupID, 0, 0, "Unfollow");
			followButton.setIcon(R.drawable.star).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
		if(item==followButton){
			//Check for Internet and location before proceeding
			if(!Manager.getInstance().checkConnectionAndLocationBeforeProceeding(TopicActivity.this)){
				return false;
			}
			
			boolean userFollows = false;
			if(Manager.getInstance().getFollowingThreadWithID(Manager.getInstance().currentThread.threadID)!=null){
				userFollows = true;
			}
			
			//To follow or unfollow
			if(userFollows==false){
				Manager.getInstance().followThread(Manager.getInstance().currentThread.threadID);
				followButton.setTitle("Unfollow");
				followButton.setIcon(R.drawable.star);
			}else{
				Manager.getInstance().unFollowThread(Manager.getInstance().currentThread.threadID);
				followButton.setTitle("Follow");
				followButton.setIcon(R.drawable.empty_star);
			}
			
			Manager.getInstance().getFollowingThreadsFromServer();
		}
		return true;
	}
	
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Manager.getInstance().removeCommentObserver();
	}
	
	public void pullFromManager(){
		resetView();
	}
}
