package com.CS4274.anonymous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.CS4274.anonymous.server.CommentsCallback;
import com.CS4274.anonymous.server.ServerCallback;
import com.CS4274.anonymous.server.ThreadsCallback;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;


public class Manager {
	private static Manager instance;
	public String phoneID; //set by main activity
	LocationManager locationManager;
	LocationDelegate locationDelegate;
	public boolean bol_GPSLocationEnabled = true; //will auto call onProviderDisabled if user disables GPS
	public boolean bol_WIFILocationEnabled = true; //will auto call onProviderDisabled if user disables GPS
	public double latestLongitude=0,latestLatitude=0;
	public double syncLongitude=0,syncLatitude=0;
	public Activity mainContext;
	public ArrayAdapter<Thread> threadsAroundAdapter;
	public ArrayAdapter<Thread> threadsFollowedAdapter;
	public Thread currentThread;
	
	//Store data
	ArrayList<Thread> threadArray; //contains threads currently around you
	ArrayList<Thread> followedArray; //contains threads that you have followed
	
	ArrayList<ServerCallback> callbackArray; //contains callback for follows and unfollow (so that i can know when to grab from the followed threads from server without overwriting the pre-callback functions)
	
	//Oberservers
	CommentObserver _commentOberserver;

	private Manager(){
		locationDelegate = new LocationDelegate(); 
		threadArray = new ArrayList<Thread>();
		followedArray = new ArrayList<Thread>();
		callbackArray = new ArrayList<ServerCallback>();
	}
	
	public void setCommentObserver(CommentObserver observer){
		_commentOberserver = observer;
	}
	
	public void removeCommentObserver(){
		_commentOberserver = null;
	}
	
	public void initLocationProviders(){
		if(locationManager==null){
			Log.e(this.getClass().getName(),"initLocationProviders but locationManager was null");
			return;
		}
				
		//Use both location provider
		initGPSProvider();
		initNetworkLocationProvider();
		
		if(bol_GPSLocationEnabled==true || bol_WIFILocationEnabled==true){
			Location tempLocation = getLastKnownLocation();
			if(tempLocation!=null){
				latestLongitude = tempLocation.getLongitude();
				latestLatitude = tempLocation.getLatitude();
				Server.getInstance().getThreads(latestLongitude, latestLatitude, new ThreadsCallback(){
					@Override
					public void success(ArrayList<Thread> thread) {
						Log.i(this.getClass().getName(),"initLocationProviders getThread");
						Manager.getInstance().updateThreads(thread);
					}
		
					@Override
					public void failure(String error) {
						Log.e(this.getClass().getName(),"initLocationProviders: Unable to getThreads from Server");
					}
				});
			}
		}else{
			locationManager = null;
		}
	}
	
	public void initGPSProvider(){
		//Set location listener
		if(locationManager!=null){
			try{
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, locationDelegate);
				if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					bol_GPSLocationEnabled=true;
				}else{
					bol_GPSLocationEnabled=false;
				}
				Log.i("LocationManager","init with GPS");
			}catch(Error e){
				Log.e(this.getClass().getName(),"could not initGPSProvider: "+e);
			}
		}else{
			Log.e(this.getClass().getName(),"locationManager was null");
		}
	}
	
	public void initNetworkLocationProvider(){
		//Set location listener
		if(locationManager!=null){
			try{
				locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 0, locationDelegate);
				if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
					bol_WIFILocationEnabled=true;
				}else{
					bol_WIFILocationEnabled=false;
				}
				Log.i("LocationManager","init with Wifi");
			}catch(Error e){
				Log.e(this.getClass().getName(),"could not initNetworkLocationProvider: "+e);
			}
		}else{
			Log.e(this.getClass().getName(),"locationManager was null");
		}
	}
	
	public static Manager getInstance(){
		if(instance==null){
			instance = new Manager();
		}
		return instance;
	}
	
	public void updateThreads(ArrayList<Thread> arrayThreads){
		threadArray.clear();
		threadArray.addAll(arrayThreads);
		ArrayList<Thread> sortedArray = mergeSort(threadArray);
		threadArray.clear();
		threadArray.addAll(sortedArray);
		
		refreshThreadsAroundView();
	}
	
	private ArrayList<Thread> mergeSort(ArrayList<Thread> arrayThreads){
		double weightOfCreated = 0.3;
		double weightOfUpdated = 0.5;
		int middle = arrayThreads.size()/2;
		if(arrayThreads.size()<=1){
			return arrayThreads;
		}else{
			ArrayList<Thread> leftArray = new ArrayList<Thread>(arrayThreads.subList(0, middle));
			ArrayList<Thread> rightArray = new ArrayList<Thread>(arrayThreads.subList(middle, arrayThreads.size()));
			leftArray = mergeSort(leftArray);
			rightArray = mergeSort(rightArray);
			ArrayList<Thread> returnArray = new ArrayList<Thread>();
			
			//Comparison
			while(!rightArray.isEmpty() && !leftArray.isEmpty()){
				double leftWeight = leftArray.get(0).dateCreated.getTime()*weightOfCreated + leftArray.get(0).dateUpdated.getTime()*weightOfUpdated;
				double rightWeight = rightArray.get(0).dateCreated.getTime()*weightOfCreated + rightArray.get(0).dateUpdated.getTime()*weightOfUpdated;
				
				if(leftWeight>rightWeight){
					returnArray.add(leftArray.get(0));
					leftArray.remove(0);
				}else{
					returnArray.add(rightArray.get(0));
					rightArray.remove(0);
				}
			}
			
			//Just add
			if(!leftArray.isEmpty()){
				returnArray.addAll(leftArray);
				leftArray.clear();
			}
			
			//Just add
			if(!rightArray.isEmpty()){
				returnArray.addAll(rightArray);
				rightArray.clear();
			}
			
			return returnArray;
		}	
	}
	
	
	public Location getLastKnownLocation(){
		Criteria criteria = new Criteria();
	    String bestProvider = locationManager.getBestProvider(criteria, true);
	    if(locationManager.getLastKnownLocation(bestProvider)!=null){
	    	return locationManager.getLastKnownLocation(bestProvider);
	    }else if(latestLongitude!=0 && latestLatitude!=0){
	    	Location tempLocation = new Location(LocationManager.PASSIVE_PROVIDER);
	    	tempLocation.setLatitude(latestLatitude);
	    	tempLocation.setLongitude(latestLongitude);
	    	return tempLocation;
	    }
	    return null;
	}
	
	public void createThread(String threadTitle, String threadFirstComment){
		final Location lastLoc = getLastKnownLocation();
		Toast toast = Toast.makeText(mainContext, "Creating New Thread...", Toast.LENGTH_LONG);
		toast.show();
		Server.getInstance().createThread(lastLoc.getLongitude(),lastLoc.getLatitude(),threadTitle,threadFirstComment, new ServerCallback(){
			@Override
			public void success() {
				Log.i(this.getClass().getName(),"Successfully created a new thread");
				Toast toast = Toast.makeText(mainContext, "Thread created, refreshing Thread list...", Toast.LENGTH_SHORT);
				toast.show();
				//Auto grab from server again
				Server.getInstance().getThreads(lastLoc.getLongitude(),lastLoc.getLatitude(), new ThreadsCallback(){
					@Override
					public void success(ArrayList<Thread> thread) {
						Log.i(this.getClass().getName(),"updateThreads");
						Manager.getInstance().updateThreads(thread);
						refreshThreadsAroundView();
						Toast toast = Toast.makeText(mainContext, "Thread list refreshed", Toast.LENGTH_SHORT);
						toast.show();
					}
		
					@Override
					public void failure(String error) {
						Log.e(this.getClass().getName(),"Unable to getThreads from Server");
					}
				});
			}

			@Override
			public void failure(String error) {
				Log.e(this.getClass().getName(),"Failed to create a new thread");
			}
		});
	}
	
	public void viewCommentsOnThread(final String threadID){
		Server.getInstance().getComments(threadID, new CommentsCallback(){
			@Override
			public void success(ArrayList<Comment> comment) {
				Log.i(this.getClass().getName(),"viewCommentsOnThread");
				boolean bol_found = false;
				
				for(int i=0;i<followedArray.size();i++){
					if(followedArray.get(i).threadID.compareToIgnoreCase(threadID)==0){
						followedArray.get(i).commentList = comment;
						Log.i("Manager ","ViewCommentsOnThread commentList size: "+comment.size());
						if(_commentOberserver!=null){
							_commentOberserver.pullFromManager();
						}
						bol_found = true;
						break;
						//Do not return after followedArray, thread might be around user location as well
					}
				}
				
				for(int i=0;i<threadArray.size();i++){
					if(threadArray.get(i).threadID.compareToIgnoreCase(threadID)==0){
						threadArray.get(i).commentList = comment;
						if(_commentOberserver!=null){
							_commentOberserver.pullFromManager();
						}
						bol_found = true;
						break;
					}
				}
				
				if(bol_found==false){
					Log.e(this.getClass().getName(),"cannot find similar threadID");
				}
			}
			@Override
			public void failure(String error) {
				Log.e(this.getClass().getName(),"cannot perform viewCommentsOnThread");
			}
			
		});
	}

	public void commentOnThread(final String threadID, String commentContent){
		Server.getInstance().commentOnThread(threadID, commentContent, new ServerCallback(){
			@Override
			public void success() {
				Log.i(this.getClass().getName(),"Successfully commented on a thread");
				//After successfully commenting, update the comments on thread
				viewCommentsOnThread(threadID);
			}

			@Override
			public void failure(String error) {
				Log.e(this.getClass().getName(),"Failed to comment on a thread:"+error);
			}
		});
	}

	public void followThread(final String threadID){
		//Auto add to followedArray
		Thread tempThread = Manager.getInstance().getThreadWithID(threadID);
		if(tempThread!=null){
			followedArray.add(tempThread);
			refreshFollowedThreadsView();
		}

		ServerCallback tempCallback = new ServerCallback(){
			@Override
			public void success() {
				Log.i(this.getClass().getName(),"Successfully followed a thread");
				callbackArray.remove(this);
			}

			@Override
			public void failure(String error) {
				Log.e(this.getClass().getName(),"Failed to follow thread");
				
				Toast toast = Toast.makeText(mainContext, "Failed to follow thread", Toast.LENGTH_LONG);
				toast.show();
				
				//Auto remove from followedArray
				Thread removeThread = Manager.getInstance().getFollowingThreadWithID(threadID);
				if(removeThread!=null){
					followedArray.remove(removeThread);
					refreshFollowedThreadsView();
				}
				
				callbackArray.remove(this);
			}	
		};
				
		callbackArray.add(tempCallback);
		
		Server.getInstance().followThread(threadID,tempCallback);
	}
	
	public void unFollowThread(final String threadID){
		//Auto remove from followedArray
		Thread removeThread = Manager.getInstance().getFollowingThreadWithID(threadID);
		if(removeThread!=null){
			followedArray.remove(removeThread);
			refreshFollowedThreadsView();
		}
		
		ServerCallback tempCallback = new ServerCallback(){
			@Override
			public void success() {
				Log.i(this.getClass().getName(),"Successfully unfollowed a thread");
				callbackArray.remove(this);
			}

			@Override
			public void failure(String error) {
				Log.e(this.getClass().getName(),"Failed to unfollow thread");
				
				Toast toast = Toast.makeText(mainContext, "Failed to unfollow thread", Toast.LENGTH_LONG);
				toast.show();
				
				//Auto add to followedArray
				Thread tempThread = Manager.getInstance().getThreadWithID(threadID);
				if(tempThread!=null){
					followedArray.add(tempThread);
					refreshFollowedThreadsView();
				}
				callbackArray.remove(this);
			}
		};
		
		callbackArray.add(tempCallback);
		
		Server.getInstance().unFollowThread(threadID,tempCallback);
	}

	//this does not connect to server, only accesses threadArray
	public Thread getThreadWithID(String threadID){
		for(int i=0;i<threadArray.size();i++){
			if(threadArray.get(i).threadID.compareTo(threadID)==0){
				return threadArray.get(i);
			}
		}
		return null;
	}
	
	
	//this does not connect to server, only accesses followedArray
	public Thread getFollowingThreadWithID(String threadID){
		for(int i=0;i<followedArray.size();i++){
			if(followedArray.get(i).threadID.compareTo(threadID)==0){
				return followedArray.get(i);
			}
		}
		return null;
	}
	
	private void refreshFollowedThreadsView(){
		if(Manager.getInstance().threadsFollowedAdapter!=null){
			Manager.getInstance().threadsFollowedAdapter.notifyDataSetChanged();
		}
	}
	
	private void refreshThreadsAroundView(){
		if(Manager.getInstance().threadsAroundAdapter!=null){
			Manager.getInstance().threadsAroundAdapter.notifyDataSetChanged();
		}
	}
	
	//Normal getThread will be done when location is updated
	
	public void getFollowingThreadsFromServer(){
		if(!callbackArray.isEmpty()){
			Log.i(this.getClass().getName(),"Ignored getFollowingThreadsFromServer due to still performing following/unfollowing");
			return;
		}
		
		Server.getInstance().getFollowingThreads(new ThreadsCallback(){
			@Override
			public void success(ArrayList<Thread> thread) {
				followedArray.clear();
				followedArray.addAll(thread);
				refreshFollowedThreadsView();
				Log.i(this.getClass().getName(),"Successfully get following threads");
			}

			@Override
			public void failure(String error) {
				Log.i(this.getClass().getName(),"Failed to get following threads");
			}
		});
	}

	public boolean checkConnectionAndLocationBeforeProceeding(Context context){
		if(isNetworkAvailable(context)==false){
			Toast toast = Toast.makeText(context, "Network unavailable, unable to proceed with request", Toast.LENGTH_LONG);
    		toast.show();
			return false;
		}
		
		if(bol_GPSLocationEnabled==false && bol_WIFILocationEnabled==false){
			Toast toast = Toast.makeText(context, "Location Service unavailable, unable to proceed with request", Toast.LENGTH_LONG);
    		toast.show();
			return false;
		}
		
		if(getLastKnownLocation()==null){
			Toast toast = Toast.makeText(context, "Unable to get last known location, unable to proceed with request", Toast.LENGTH_LONG);
    		toast.show();
			return false;
		}
		
		return true;
	}
	
	public static boolean isNetworkAvailable(Context context){
	    return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
	}
}
