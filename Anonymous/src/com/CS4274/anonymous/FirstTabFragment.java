package com.CS4274.anonymous;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;

import com.CS4274.anonymous.server.ThreadsCallback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockListFragment;


public class FirstTabFragment extends SherlockListFragment implements ActionBar.TabListener{    
	MenuItem newThreadButton;
	MenuItem refreshButton;
	Dialog dialog_createThread;
	/** An array of items to display in ArrayList */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	
    	if(Manager.getInstance().threadsAroundAdapter==null){
    		/** Creating array adapter to set data in listview */
    		Manager.getInstance().threadsAroundAdapter = new ArrayAdapter<Thread>(getActivity(), android.R.layout.simple_expandable_list_item_1, Manager.getInstance().threadArray);
    	}
        /** Setting the array adapter to the listview */
        setListAdapter(Manager.getInstance().threadsAroundAdapter);
        
        setHasOptionsMenu(true);
        
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void onListItemClick (ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	
    	Intent intent = new Intent(getActivity(), TopicActivity.class);
    	intent.putExtra("index", position);
    	intent.putExtra("arrayType", "threadArray");
    	Manager.getInstance().viewCommentsOnThread(Manager.getInstance().threadArray.get(position).threadID);
    	startActivity(intent);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	//Setting the multiselect choice mode for the listview
        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);

    }

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {		
		ft.add(android.R.id.content, this,"android");
		ft.attach(this);
	}
		
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.detach(this);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	
	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
		super.onCreateOptionsMenu(menu, inflater);
		int groupID = 1;
		//Remove any previous icons
		menu.removeGroup(groupID);
		newThreadButton = menu.add(groupID, 0, 0, "Create New Topic");
		newThreadButton.setIcon(R.drawable.new_thread).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		refreshButton = menu.add(groupID, 0, 1, "Refresh Topics");
		refreshButton.setIcon(R.drawable.refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item == newThreadButton){
			
			//Check for Internet and location before proceeding
			if(!Manager.getInstance().checkConnectionAndLocationBeforeProceeding(getActivity())){
				return false;
			}
			
			 // Create custom dialog object
			dialog_createThread = new Dialog(getActivity());
            // Include dialog.xml file
			dialog_createThread.setContentView(R.layout.create_thread);
            // Set dialog title
			dialog_createThread.setTitle("Create New Topic");
            
			dialog_createThread.show();
            
            Button btn_submit = (Button) dialog_createThread.findViewById(R.id.createThread_submit);
            Button btn_cancel = (Button) dialog_createThread.findViewById(R.id.createThread_cancel);
            final EditText title = (EditText) dialog_createThread.findViewById(R.id.createThread_title);
            final EditText content = (EditText) dialog_createThread.findViewById(R.id.createThread_content);
            
            btn_cancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                	dialog_createThread.dismiss();
                }
            });
            
            btn_submit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	//Check for Internet and location before proceeding
        			if(!Manager.getInstance().checkConnectionAndLocationBeforeProceeding(getActivity())){
        				return;
        			}
                	
                	title.setText(title.getText().toString().trim());
                	content.setText(content.getText().toString().trim());
                	if(title.getText().toString().length()==0){
                		Toast toast = Toast.makeText(getActivity(), "Title cannot be empty", Toast.LENGTH_SHORT);
                		toast.show();
                		return;
                	}
                	
                	if(content.getText().toString().length()==0){
                		Toast toast = Toast.makeText(getActivity(), "Content cannot be empty", Toast.LENGTH_SHORT);
                		toast.show();
                		return;
                	}
                	Manager.getInstance().createThread(title.getText().toString(), content.getText().toString());
                    // Close dialog
                	dialog_createThread.dismiss();
                }
            });
            
			return true;
			
		}else if(item == refreshButton){
			//Check for Internet and location before proceeding
			if(!Manager.getInstance().checkConnectionAndLocationBeforeProceeding(getActivity())){
				return false;
			}
			
			Location tempLoc = Manager.getInstance().getLastKnownLocation();
			if(tempLoc!=null){
				Server.getInstance().getThreads(tempLoc.getLongitude(), tempLoc.getLatitude(), new ThreadsCallback(){

					@Override
					public void success(ArrayList<Thread> thread) {
						Manager.getInstance().updateThreads(thread);
						Toast toast = Toast.makeText(getActivity(), "Topic list updated", Toast.LENGTH_SHORT);
						toast.show();
					}

					@Override
					public void failure(String error) {
						Toast toast = Toast.makeText(getActivity(), "Unable to get refresh topic list", Toast.LENGTH_SHORT);
						toast.show();
					}
				});
					
				
				Toast toast = Toast.makeText(getActivity(), "Refreshing...", Toast.LENGTH_SHORT);
				toast.show();
			}else{
				Toast toast = Toast.makeText(getActivity(), "Unable to get current location, please try again later.", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		return super.onOptionsItemSelected(item);
	}
}