package com.CS4274.anonymous;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockListFragment;

public class SecondTabFragment extends SherlockListFragment  implements ActionBar.TabListener{
	    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){  
		/** Creating array adapter to set data in listview */
		if(Manager.getInstance().threadsFollowedAdapter==null){
			Manager.getInstance().threadsFollowedAdapter = new ArrayAdapter<Thread>(getActivity(), android.R.layout.simple_expandable_list_item_1, Manager.getInstance().followedArray);
		}

        /** Setting the array adapter to the listview */
        setListAdapter(Manager.getInstance().threadsFollowedAdapter);
    	
        Manager.getInstance().getFollowingThreadsFromServer();
        
        //Set no actionbar items
        setHasOptionsMenu(false);
        
        return super.onCreateView(inflater, container, savedInstanceState);
    }    
    
    @Override
    public void onStart() {    	
    	super.onStart();
    	
        //Setting the multiselect choice mode for the listview
        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);

    }
    
    @Override
    public void onListItemClick (ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	
    	Intent intent = new Intent(getActivity(), TopicActivity.class);
    	intent.putExtra("index", position);
    	intent.putExtra("arrayType", "followedArray");
    	Manager.getInstance().viewCommentsOnThread(Manager.getInstance().followedArray.get(position).threadID);
    	startActivity(intent);
    }
    

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.add(android.R.id.content, this,"apple");
		
		ft.attach(this);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.detach(this);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	
}